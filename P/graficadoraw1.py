#Cargar w1 function 

#############################################
#Seccion: 1. Utilizando W en varios puntos 
#############################################

data_y1 = []
data_y2 = []
data_y3 = []
data_y4 = []
data_y5 = []
data_y6 = []

data_x = list(range(0,100))

myInt = 5
data_x = [x / myInt for x in data_x]

for n in range(0,100):
  t=n/myInt
  data_y1 = data_y1+ [float(w1_function(i=1,t=10,xi=t,la_num=0.2,mu_num=0.5))]
  data_y2 = data_y2+ [float(w1_function(i=2,t=10,xi=t,la_num=0.2,mu_num=0.5))]
  data_y3 = data_y3+ [float(w1_function(i=3,t=10,xi=t,la_num=0.2,mu_num=0.5))]
  data_y4 = data_y4+ [float(w1_function(i=4,t=10,xi=t,la_num=0.2,mu_num=0.5))]
  data_y5 = data_y5+ [float(w1_function(i=5,t=10,xi=t,la_num=0.2,mu_num=0.5))]
  data_y6 = data_y6+ [float(w1_function(i=6,t=10,xi=t,la_num=0.2,mu_num=0.5))]
  
numpy.trapz(y=data_y1,x=data_x)
numpy.trapz(y=data_y2,x=data_x)
numpy.trapz(y=data_y3,x=data_x)
numpy.trapz(y=data_y4,x=data_x)
numpy.trapz(y=data_y5,x=data_x)
numpy.trapz(y=data_y6,x=data_x)

