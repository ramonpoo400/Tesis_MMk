#Descripción:
# Esta parte correpsonde al capitulo4 seccion 1 de mi tesis,
# consiste en graficar la función de masa de probabilidad del numeor de clientes
# que llegan a la cola durante un periodo de servicio con un servidor

#Importando libreria sympy 
from sympy import *
from numpy import *
from sympy import sqrt
import sympy
import numpy
  
def P_N(la_num,mu_num,m):
  
  data_x = [-1+i/100 for i in range(201)]
  
  data_y = []
  
  for variable in data_x:
    data_y = data_y + [variable**(2*m)*sqrt(1-variable**2)]
    
  integral = numpy.trapz(x=data_x,y=data_y)  
  
  P = ((2*mu_num)/(la_num+mu_num))*((sqrt(4*la_num*mu_num)/(la_num+mu_num))**(2*m))*(1/numpy.pi)*integral
  
  return(float(P))  
