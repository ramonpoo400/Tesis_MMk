#Descripción
# Esta parte correpsonde al capitulo4 seccion 1 de mi tesis,
# consiste en graficar la función de densidad FN1 (distribución longitud de periodo serv 1 serv)

#Importando libreria sympy 
from sympy import *
from numpy import *
from sympy import sqrt
import sympy
import numpy
  
def F_function(la_num,mu_num,t_num,n_num):  
  
  #Declarqndo symbolos en sympy
  x,la,mu,t,n = symbols('x lambda mu t n')  
  
  theta = acos((la+mu-x)/(sqrt(4*la*mu)))
  
  # Definineod numero de servidores
    
  k=1
  
  # Definiendo integrando simbolico 
  
  integrando_sim = (1-e**(-t*(la+mu-sqrt(4*la*mu))))/(la+mu-sqrt(4*la*mu)*sympy.cos(theta))*sympy.sin(n*theta)
  
  I1_num = la_num+k*mu_num-sqrt(4*k*la_num*mu_num).evalf()
  I2_num = la_num+k*mu_num+sqrt(4*k*la_num*mu_num).evalf()
  
  integrando_num = integrando_sim.subs(la,la_num).subs(mu,mu_num).subs(t,t_num).subs(n,n_num)
  
  integrando_fun = lambdify(x,integrando_num,"numpy")
  
  data_x = linspace(start = float(I1_num)+0.000001,stop = float(I2_num)-0.000001,num = 100000)
      
  data_y = integrando_fun(data_x)
  
  integral = numpy.trapz(y=data_y,x=data_x)
  
  F = (1/numpy.pi)*sqrt(mu_num/la_num)**n_num*integral
  
  return(F)
