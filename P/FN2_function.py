#Este script sitrve para calcular la distribución del numero de clientes en un periodo de servico 
# con al menos un servidor ocupado en el caso MM2 
#Importando libreria sympy 
from sympy import *
from numpy import *
from sympy import sqrt
import sympy
import numpy

def FN2_function(la,mu,m):
  x = symbols('x')
  
  p = la/(la+2*mu)
  p1 = la/(la+mu)
  q = (2*mu)/(la+2*mu)
  q1 = mu/(la+mu)
  m1 = p1*sqrt(q/(p1-p))
  
  I1 = -sqrt(4*p*q)
  I2 = sqrt(4*p*q)
  
  hat_psi = (1/numpy.pi)*((sqrt((8*la*mu)/(la+2*mu)**2-x**2))/(x**2*(-2*mu/(la+2*mu))+(4*la*mu/((la+mu)*(la+2*mu)))))
  
  integrando = lambdify(x,x**(2*m)*hat_psi,"numpy")
  
  data_x = linspace(start = float(I1)+0.00001,stop = float(I2)-0.00001,num = 1000)
  data_y = []
    
  for var in data_x:
    data_y = data_y +[float(integrando(var))]
    
  integral = numpy.trapz(x=data_x,y=data_y)
  
  Proba = (mu/(la+mu))*integral
  
  return(float(Proba))
