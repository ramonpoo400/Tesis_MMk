  
  #Descripción
  # Esta parte correpsonde al capitulo4 seccion 2 de mi tesis,
  # consiste en graficar la función de densidad PT1 (dusrción del periodo de servicio con un servidor
  # ocupado). Solo funciona para 2la>=mu!!! 
  
  #Importando libreria sympy 
  from sympy import *
  from numpy import *
  from sympy import sqrt
  import sympy
  import numpy
  
  
  
  def PT1_function(la_num,mu_num,t_num):
    #Declarqndo symbolos en sympy
    x,la,mu,t = symbols('x lambda mu t')  
    
    # Definineod numero de servidores
      
    k=2
    
    # Definiendo integrando simbolico 
    
    integrando_sim = ((1-e**(-x*t))/(x))*(sqrt(8*la*mu-(la+2*mu-x)**2)/(x-(mu-la)))
    
    I1_num = la_num+k*mu_num-sqrt(4*k*la_num*mu_num).evalf()
    I2_num = la_num+k*mu_num+sqrt(4*k*la_num*mu_num).evalf()
    
    integrando_num = integrando_sim.subs(la,la_num).subs(mu,mu_num).subs(t,t_num)
    
    integrando_fun = lambdify(x,integrando_num,"numpy")
    
    data_x = linspace(start = float(I1_num)+0.000001,stop = float(I2_num)-0.000001,num = 100000)
        
    data_y = integrando_fun(data_x)
    
    integral = numpy.trapz(y=data_y,x=data_x)
    
    puntual = (mu_num-2*la_num)*(1-e**(-(mu_num-la_num)*t_num))/(mu_num-la_num)
    
    PT1 = puntual + (1/(2*numpy.pi))*integral
    
    return(float(PT1))
