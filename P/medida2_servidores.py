#Descripción
# Esta parte correpsonde al capitulo3 seccion 1 de mi tesis,
# consiste en calcular la medida asociada al proceso de colas de un servidor

#Importando libreria sympy 
from sympy import *
from numpy import *
from sympy import sqrt
import sympy
import numpy

#Declarqndo symbolos en sympy
x,y,la,mu, xi,a,b = symbols('x y lambda mu xi a b')  

# Definineod numero de servidores

k=2

#Definiendo los polinomios de nacimiento y muerte 1-servidores 

Q_0 = 1
Q_1 = (la-x)/la

n=1
Q_n = Q_1
Q_n_1 = Q_0

Q_2 = (1/la)*((la+mu-x)*Q_n -mu*Q_n_1)

n=2
Q_n = Q_2
Q_n_1 = Q_1

Q_3 = (1/la)*((la+2*mu-x)*Q_n -2*mu*Q_n_1)

n=3
Q_n = Q_3
Q_n_1 = Q_2

Q_4 = (1/la)*((la+2*mu-x)*Q_n -2*mu*Q_n_1)

n=4
Q_n = Q_4
Q_n_1 = Q_3

Q_5 = (1/la)*((la+2*mu-x)*Q_n-2*mu*Q_n_1)

n=5
Q_n = Q_5
Q_n_1 = Q_4

Q_6 = (1/la)*((la+2*mu-x)*Q_n-2*mu*Q_n_1)

n=6
Q_n = Q_6
Q_n_1 = Q_5

Q_7 = (1/la)*((la+2*mu-x)*Q_n-2*mu*Q_n_1)

n=7
Q_n = Q_7
Q_n_1 = Q_6

Q_8 = (1/la)*((la+2*mu-x)*Q_n-2*mu*Q_n_1)

n=8
Q_n = Q_8
Q_n_1 = Q_7

Q_9 = (1/la)*((la+2*mu-x)*Q_n-2*mu*Q_n_1)

n=9
Q_n = Q_9
Q_n_1 = Q_8

Q_10 = (1/la)*((la+2*mu-x)*Q_n-2*mu*Q_n_1)

lista_poly = [Q_0,Q_1,Q_2,Q_3,Q_4,Q_5,Q_6,Q_7,Q_8,Q_9,Q_10] 

#Definiendo los polinomios 0-asociados del proceso de nacimiento y muerte 1-servidores 

Q0_0 = 0
Q0_1 = -1/la

n=1
Q0_n = Q0_1
Q0_n_1 = Q0_0

Q0_2 = (1/la)*((la+mu-x)*Q0_n-mu*Q0_n_1) 

n=2
Q0_n=Q0_2
Q0_n_1=Q0_1

Q0_3 = (1/la)*((la+2*mu-x)*Q0_n-2*mu*Q0_n_1) 

n=3
Q0_n = Q0_3
Q0_n_1 = Q0_2

Q0_4 = (1/la)*((la+2*mu-x)*Q0_n-2*mu*Q0_n_1)

n=4
Q0_n = Q0_4
Q0_n_1 = Q0_3

Q0_5 = (1/la)*((la+2*mu-x)*Q0_n-2*mu*Q0_n_1)

n=5
Q0_n = Q0_5
Q0_n_1 = Q0_4

Q0_6 = (1/la)*((la+2*mu-x)*Q0_n-2*mu*Q0_n_1)

n=6
Q0_n = Q0_6
Q0_n_1 = Q0_5

Q0_7 = (1/la)*((la+2*mu-x)*Q0_n-2*mu*Q0_n_1)

n=7
Q0_n = Q0_7
Q0_n_1 = Q0_6

Q0_8 = (1/la)*((la+2*mu-x)*Q0_n-2*mu*Q0_n_1)

n=8
Q0_n = Q0_8
Q0_n_1 = Q0_7

Q0_9 = (1/la)*((la+2*mu-x)*Q0_n-2*mu*Q0_n_1)

n=9
Q0_n = Q0_9
Q0_n_1 = Q0_8

Q0_10 = (1/la)*((la+2*mu-x)*Q0_n-2*mu*Q0_n_1)

#Calculamos la transformada de Stieltjes del proceso 

L2P = Poly((4*la**2)*Q_2*Q0_2+4*k*la*mu*Q_1*Q0_1-2*la*(la+k*mu-x)*(Q_2*Q0_1+Q_1*Q0_2),x).expr#parte polinomial
L2R = 2*factorial(k-1)*(mu/la)**(k-1)*sympy.sqrt((la+k*mu-x)**2-4*k*la*mu)#parte radical 

print(latex(Poly(L2P/(4*mu/la**4),x)))  #se imprime la parte polinomial le L2 dividida entre (4*mu/la**4)
print(latex(L2R)) #imprime la parte racinal de L2


L2 = L2P+L2R

H2 = Poly(4*k*la*mu*((Q_1**2))-4*la*mu*Q_2*(Q_1),x).expr

B2z = -L2/H2


# Calculando la parte absolutamente continua de la medida 

psi_hat = (factorial(k-1)/(2*sympy.pi*la**2))*(mu/la)**(k-1)*sympy.sqrt(4*la*k*mu-(la+k*mu-x)**2)/simplyfy(Q_1**2-Q_0*Q_2)

print(latex(psi_hat))

#Calculando intervalo de coninuidad

I1 = la+k*mu-sqrt(4*k*la*mu)
I2 = la+k*mu+sqrt(4*k*la*mu)


#Calculamos G^(-1)
 
G_2 = (xi**(-k)*(Q_2.subs(x,la*(1-xi*sqrt(k))**2)-xi*sqrt(k)*Q_1.subs(x,la*(1-xi*sqrt(k))**2))).subs(mu/la,xi**2)

print(latex(simplify(G_2)))

solve(2*x**2-4*sqrt(2)*x+3)
