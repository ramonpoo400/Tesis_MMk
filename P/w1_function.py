#Descripción
# Esta parte correpsonde al capitulo4 seccion 1 de mi tesis,
# consiste en graficar la función de densidad w_i(t,xi) para valores dados 

#Importando libreria sympy 
from sympy import *
from numpy import *
from sympy import sqrt
import sympy
import numpy

def w1_function(i,t,xi,la_num,mu_num):
  
  #Declarqndo symbolos en sympy
  x,y,la,mu,a,b = symbols('x y lambda mu a b')  
  
  # Definineod numero de servidores
  
  k=1
  
  #Definiendo los polinomios de nacimiento y muerte 1-servidores 
  
  Q_0 = 1
  Q_1 = (la-x)/la
  
  n=1
  Q_n = Q_1
  Q_n_1 = Q_0
  
  Q_2 = (1/la)*((la+mu-x)*Q_n -mu*Q_n_1)
  
  n=2
  Q_n = Q_2
  Q_n_1 = Q_1
  
  Q_3 = (1/la)*((la+mu-x)*Q_n -mu*Q_n_1)
  
  n=3
  Q_n = Q_3
  Q_n_1 = Q_2
  
  Q_4 = (1/la)*((la+mu-x)*Q_n -mu*Q_n_1)
  
  n=4
  Q_n = Q_4
  Q_n_1 = Q_3
  
  Q_5 = (1/la)*((la+mu-x)*Q_n-mu*Q_n_1)
  
  n=5
  Q_n = Q_5
  Q_n_1 = Q_4
  
  Q_6 = (1/la)*((la+mu-x)*Q_n-mu*Q_n_1)
  
  n=6
  Q_n = Q_6
  Q_n_1 = Q_5
  
  Q_7 = (1/la)*((la+mu-x)*Q_n-mu*Q_n_1)
  
  n=7
  Q_n = Q_7
  Q_n_1 = Q_6
  
  Q_8 = (1/la)*((la+mu-x)*Q_n-mu*Q_n_1)
  
  n=8
  Q_n = Q_8
  Q_n_1 = Q_7
  
  Q_9 = (1/la)*((la+mu-x)*Q_n-mu*Q_n_1)
  
  n=9
  Q_n = Q_9
  Q_n_1 = Q_8
  
  Q_10 = (1/la)*((la+mu-x)*Q_n-mu*Q_n_1)
  
  lista_poly = [Q_0,Q_1,Q_2,Q_3,Q_4,Q_5,Q_6,Q_7,Q_8,Q_9,Q_10] 
  
  I1_num = la_num+k*mu_num-sqrt(4*k*la_num*mu_num).evalf()
  I2_num = la_num+k*mu_num+sqrt(4*k*la_num*mu_num).evalf()
  
  #Calculando parte integral 
  
  integrando = ((e**(-x*t)*lista_poly[i].subs(la,la_num).subs(mu,mu_num))/x)*sympy.im(((la_num-x)*e**(1j*(acos((la_num+mu_num-x)/sqrt(4*la_num*mu_num))))-sqrt(la_num*mu_num))*e**(xi*sqrt(la_num*mu_num)*e**(1j*(acos((la_num+mu_num-x)/sqrt(4*la_num*mu_num))))))
  
  f = lambdify(x,integrando,"numpy") 
  
  data_x = linspace(start = float(I1_num)+0.000001,stop = float(I2_num)-0.000001,num = 100000)
    
  data_y = f(data_x)
    
  integral =  numpy.trapz(y=data_y,x=data_x)
  
  #Calculando parte puntual 
  
  puntual = (la_num/mu_num)*(mu_num-la_num)*e**(-(mu_num-la_num)*xi)
  
  #Sumando ambas partes en formula w1
  
  w = puntual + e**(-mu_num*xi)*sqrt(la_num/mu_num)*(1/numpy.pi)*integral
  
  return(w)
