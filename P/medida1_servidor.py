#Descripción
# Esta parte correpsonde al capitulo3 seccion 1 de mi tesis,
# consiste en calcular la medida asociada al proceso de colas de un servidor

#Importando libreria sympy 
from sympy import *
from numpy import *
from sympy import sqrt
import sympy
import numpy

#Declarqndo symbolos en sympy
x,y,la,mu, xi,a,b = symbols('x y lambda mu xi a b')  

# Definineod numero de servidores

k=1

#Definiendo los polinomios de nacimiento y muerte 1-servidores 

Q_0 = 1
Q_1 = (la-x)/la

n=1
Q_n = Q_1
Q_n_1 = Q_0

Q_2 = (1/la)*((la+mu-x)*Q_n -mu*Q_n_1)

n=2
Q_n = Q_2
Q_n_1 = Q_1

Q_3 = (1/la)*((la+mu-x)*Q_n -mu*Q_n_1)

n=3
Q_n = Q_3
Q_n_1 = Q_2

Q_4 = (1/la)*((la+mu-x)*Q_n -mu*Q_n_1)

#Definiendo los polinomios 0-asociados del proceso de nacimiento y muerte 1-servidores 

Q0_0 = 0
Q0_1 = -1/la

n=1
Q0_n = Q0_1
Q0_n_1 = Q0_0

Q0_2 = (1/la)*((la+mu-x)*Q0_n-mu*Q0_n_1) 

n=2
Q0_n=Q0_2
Q0_n_1=Q0_1

Q0_3 = (1/la)*((la+mu-x)*Q0_n-mu*Q0_n_1) 

n=3
Q0_n = Q0_3
Q0_n_1 = Q0_2

Q_4 = (1/la)*((la+mu-x)*Q0_n -mu*Q0_n_1)

#Calculamos la transformada de Stieltjes del proceso 

k=1

L1P = Poly((4*la**2)*Q_1*Q0_1+4*k*la*mu*Q_0*Q0_0-2*la*(la+k*mu-x)*(Q_1*Q0_0+Q_0*Q0_1),x).expr#parte polinomial
L1R = 2*factorial(k-1)*(mu/la)**(k-1)*sympy.sqrt((la+k*mu-x)**2-4*k*la*mu)#parte radical 

print(latex(Poly(L3P/(4*mu/la**4),x)))  #se imprime la parte polinomial le L3 dividida entre (4*mu/la**4)
print(latex(L3R)) #imprime la parte racinal de L3


L1 = L1P+L1R

H1 = Poly(4*k*la*mu*((Q_0**2))-4*la*mu*Q_1*(Q_0),x).expr

B1z = -L1/H1


# Calculando la parte absolutamente continua de la medida 

psi_hat = (factorial(k-1)/(2*sympy.pi*la**2))*(mu/la)**(k-1)*sympy.sqrt(4*la*k*mu-(la+k*mu-x)**2)/simplyfy(Q_1**2-Q_0*Q_2)

print(latex(psi_hat))

#Calculando intervalo de coninuidad

I1 = la+k*mu-sqrt(4*k*la*mu)
I2 = la+k*mu+sqrt(4*k*la*mu)


#Calculamos G^(-1)
 
# G para k en general  G_1 = xi**(-k)*(Q_1.subs(x,la*(1-xi*sqrt(k))**2)-xi*sqrt(k)*Q_0.subs(x,la*(1-xi*sqrt(k))**2))

G_1 = xi**(-k)*(Q_1.subs(x,la*(1-xi*sqrt(k))**2)-xi*sqrt(k)*Q_0)

print(latex(G_1))
